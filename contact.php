<?php

$f_name = '';
$last_name = '';
$email = '';
$user = '';
$zip = '';
$country = '';
$city = '';
$adr = '';
$tel = '';
$nameoncard = '';
$cnum = '';
$exp = '';
$cvv = '';
$nameErr = '';
$lnErr = '';
$emailErr = '';
$userErr = '';
$zipErr = '';
$cvvErr = '';

function test_input($data)
{
    $data = trim($data);
    $data = stripcslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["f_name"])) {
        $nameErr = "Name is required.";
    } else {
        $name = test_input($_POST["f_name"]);
    }

    if (empty($_POST["last_name"])) {
        $lnErr = "Last name is required.";
    } else {
        $last_name = test_input($_POST["last_name"]);
    }

    if (empty($_POST["zip"])) {
        $zipErr = "Zip is required.";
    } else {
        $zip = test_input($_POST["zip"]);
    }

    if (empty($_POST["user"])) {
        $userErr = "Username is required.";
    } else {
        $user = test_input($_POST["user"]);
    }

    if (empty($_POST["cvv"])) {
        $cvvErr = "CVV is required.";
    } else {
        $cvvErr = test_input($_POST["cvv"]);
    }

    if (empty($_POST["email"])) {
        $emailErr = "You must complete email.";
    } elseif (!empty($_POST["email"]) && !filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
        $emailErr = "Invalid email format!";
    } else {
        $email = test_input($_POST["email"]);
    }

    $conn =  new mysqli('localhost','root','dragan','cover');
    if ($conn->connect_error) {
        die("Connection failed:".$conn->connect_error);
    }

    $firstname = $_POST["f_name"];
    $lastname = $_POST["last_name"];
    $username = $_POST["user"];
    $mail = $_POST["email"];
    $pass = $_POST["pwd"];
    $cny = $_POST["country"];
    $city = $_POST["city"];
    $z = $_POST["zip"];
    $address = $_POST["adr"];
    $phone = $_POST["tel"];
    $noc = $_POST["nameoncard"];
    $card = $_POST["cnum"];
    $exp = $_POST["exp"];
    $cvv = $_POST["cvv"];

    $sql = "INSERT INTO members VALUES (null, '$firstname', '$lastname', '$username', '$mail', '$pass')";
    $result = $conn->query($sql);
    $last_id = $conn->insert_id;

    $sql = "INSERT INTO memaddress VALUES (null, '$cny', '$city', '$z', '$address', '$phone')";
    $result = $conn->query($sql);
    $last_id = $conn->insert_id;

    $sql = "INSERT INTO payment VALUES (null, '$noc', '$card', '$exp', '$cvv')";
    $result = $conn->query($sql);
    $last_id = $conn->insert_id;
}
?>

<!DOCTYPE hmtl>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">

	<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
                integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
                crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
                integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
                crossorigin="anonymous"></script>
		<script src=""href="assets/css/bootstrap.min.js">
		</script>
	<title>P-School/Purchase form</title>
</head>

<body>
	<div class="container">
	<nav class="navbar navbar-expand-md navbar-light bg-visible sticky-top">
		<a class="navbar-brand" href="cover.php"><img src="img/logo.png" height="80" width="150"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"> 
			<span class="navbar-toggler-icon" aria label="Toggler-navigation"></span>	
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link" href="cover.php">Home</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="pricing.php">Pricing</a>
					
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="contact.php">Purchase</a>
				</li>
 			</ul>	
 		</div>
</nav>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

<h2 class="text-center text-danger">Purchase form</h2>

	<p id="demo">Welcome to purchase form. Your packet is:
        <?php echo $_COOKIE['packet']; ?>.
	 <br>Please complete that and start the learning today!</p>

  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationServer01">First name</label>
      <input type="text" name ="f_name" class="form-control" id="validationServer01"
             placeholder="First name" > <?php echo $nameErr;?>
      
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationServer02">Last name</label>
      <input type="text" name ="last_name" class="form-control " id="validationServer02"
             placeholder="Last name" > <?php echo $lnErr;?>
		    </div>
		  </div>

	<div class="form-row">
		<div class="col-md-4 mb-3">
			<label for="user">Username</label>
			<input type="text" name="user" class="form-control" placeholder="Username">
			<?php echo $userErr;?>
		</div>

		<div class="col-md-4 mb-3">
			<label for="password">Password</label>
            <label>
                <input type="password" placeholder="Password" name="pwd" class="form-control">
            </label>

        </div>
	</div>
		<div class="form-row">
				<div class="col-md-4 mb-3">
				<label for="validationEmail1">Email address</label>
                    <input type="email" placeholder="Email address" name="email" class="form-control">
				</div>	
                <div class="col-md-4 mb-3">
                	<label>Choose country</label>
                    <label>
                        <select class="form-control"  name="country">
                                  <option>...</option>
                                  <option>Serbia</option>
                                  <option>Bosnia and Herzegovina</option>
                                  <option>Croatia</option>
                                  <option>Montenegro</option>
                                  <option>North Macedonia</option>
                                  <option>Albania</option>
                                  <option>Hungry</option>
                                  <option>Romania</option>
                                  <option>Bulgaria</option>
                        </select>
                    </label>
                </div>
		</div>
		
  <div class="form-row">
    <div class="col-md-5 mb-3">
      <label for="validationServer03">City</label>
      <input type="text" name="city" class="form-control" placeholder="City"  >
     
    </div>
    
    <div class="col-md-3 mb-3">
      <label for="validationServer05">Zip</label>
      <input type="text" class="form-control" name="zip" placeholder="Zip" > <?php echo $zipErr;?>
    </div>
  </div>

  <div class="form-row">
  	<div class="col-md-4 mb-3">
  		<label for="validationServer04">Address</label>
  		<input type="text" name="adr" class="form-control" id="validationServer04" placeholder="Address" >
  	</div>
  	<div class="col-md-4 mb-3">
  		<label for="mobilenumber">Phone Number</label>
  		<input type="text" name="tel" class="form-control" id="mobilenumber" placeholder="Phone Number" >
  </div>
</div>
  <div class="form-group">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" value="" id="invalidCheck3" required>
      <label class="form-check-label" for="invalidCheck3">
       <a href="#"> Agree to terms and conditions </a>
      </label>
      <div class="invalid-feedback">
        You must agree before submitting.
      </div>
    </div>
  </div>

  <h3 class="text-left">Payment</h3>

<div class="custom-control custom-radio">
  <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
  <label class="custom-control-label" for="customRadio1">Credit card</label>
</div>
<div class="custom-control custom-radio">
  <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
  <label class="custom-control-label" for="customRadio2">Debit card</label>
</div>
<div class="custom-control custom-radio">
  <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
  <label class="custom-control-label" for="customRadio3">Paypal</label>
</div>
<br>
<div class="form-row">
  	<div class="col-md-4 mb-3">
  		<label for="nameoncard">Name on card</label>
  		<input type="text" name="nameoncard" class="form-control" id="nameoncard" >
  	</div>
  	<div class="col-md-4 mb-3">
  		<label for="cardnumber">Card number</label>
  		<input type="text" name="cnum"  class="form-control" id="cardnumber" >
  </div>
  <div class="invalid-feedback">
					Choose payment way!
				</div>
</div>

<div class="form-row">
  	<div class="col-md-2 mb-3">
  		<label for="expiration">Expiration</label>
  		<input type="text" name="exp" class="form-control" id="expiration"  placeholder="m/y example:05/23">
  	</div>
  	<div class="col-md-2 mb-3">
  		<label for="CVV">CVV</label>
  		<input type="text" name="cvv" class="form-control" id="CVV"  placeholder="3digit" >
  		<?php echo $cvvErr;?>
  </div>
</div>
  <input name="btn "class="btn btn-primary col-md-8 mb-3" type="submit">
</form>
</div>
<footer class="text-center"> <?php echo "Today is " . date("d/m/Y") . "<br>"; ?>
</footer>	
	</body>
</html>