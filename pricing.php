<?php
session_start();
$_SESSION['submit1'] = 'Beginner';
$_SESSION['submit2'] = 'Medior';
$_SESSION['submit3'] = 'Senior';
?>

<!DOCTYPE hmtl>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
	<script src=""href="assets/css/bootstrap.min.js"></script>

	<title>P-School/Pricing</title>
</head>
<body>

	<style >
		.text-inline{font-weight: bolder; }
		.btn{border: none; color:blue; text-align: center; }
	</style>

	<div class="container">
	<nav class="navbar navbar-expand-md navbar-light bg-visible sticky-top">
		<a class="navbar-brand" href="cover.php"><img src="img/logo.png" height="80" width="150" alt=""></a> 
		
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"> 
			<span class="navbar-toggler-icon" aria label="Toggler-navigation"></span>	
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link" href="cover.php">Home</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="pricing.php">Pricing</a
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Purchase<p class="small">You must to choose some packet!</p></a>
				</li>
 			</ul>	
 		</div>
</nav>
<br>
			<h2 class="text-heading">Choose the packet for your stage!</h2>
			<br> <br><br>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

			<div class="container-fluid padding">
				<div class="row padding">
					<div class="col-md-4">
						<div class="card" name="Beginner">
							<div class="card-body">
								<h3 class="card-title">Beginner</h3>
								<p class="card-text">Programming for absolute beginners.
                                    Very productable and great to learn a basic stuff.</p>
								<p class="text-inline">5$/mo
									<a href="contact.php" onclick="setMyCookie(getPacketName(this))"
                                       class="btn btn-primary mat" name="submit1">Take it!</a>
								</p>
							</div>
						</div>
					</div>
	
					<div class="col-md-4">
						<div class="card" name="Medior">
							<div class="card-body">
								<h3 class="card-title">Medior</h3>
								<p class="card-text">For students with some little experince and for students
                                    which want to learn more and get a new stuff.</p>
								<p class="text-inline">20$/mo
									<a href="contact.php" onclick="setMyCookie(getPacketName(this))"
                                       class="btn btn-primary mat" name="submit2">Take it!</a>
								</p>
							</div>
						</div>
					</div>
			
					<div class="col-md-4">
						<div class="card" name="Senior">
							<div class="card-body">
								<h3 class="card-title">Senior</h3>
								<p class="card-text">For gradute students which want to check their experince
                                    and some forms that the didn't learn on faculty.</p>
								<p class="text-inline">35$/mo
									<a href="contact.php" onclick="setMyCookie(getPacketName(this))"
                                       class="btn btn-primary mat" name="submit3">Take it!</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<br><br>
		<p class="text-left">
			When you start with the program, you will receive our materials
            and they will be available to you anytime during the learning process.
            In the end, you will receive an official certificate, when you finish a final test.
            After that, you will also have access to the materials you have paid.
            <br>(You never know when you will need it.)
		</p>
		
</form>

<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		
<script>
	function getPacketName(element){
		return element.parentElement.parentElement.firstElementChild.innerHTML
	}
	function setMyCookie(name){
		document.cookie= "packet="+name;
	}
</script>
</body>
</html>